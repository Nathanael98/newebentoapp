import DS from 'ember-data';

export default DS.Model.extend({
    
    name: DS.attr('string'),    
    chat: DS.hasMany('chat', {inverse: 'event'}),
    tokens: DS.hasMany('token'),

});
