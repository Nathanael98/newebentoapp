import DS from 'ember-data';

export default DS.Model.extend({
    
    name: DS.attr('string'),           
    img: DS.attr('string'),
    interes: DS.attr('string'),
    nombreImg: DS.attr("string"),
    uid: DS.attr("string"),
    email: DS.attr("string"),
    pass: DS.attr("string"),
    chat: DS.hasMany('chat', {inverse: 'members'}), 
    messages: DS.hasMany('message'), 
    tokens: DS.hasMany('token'),

    //TO DO: no guardar la contraseña

});
