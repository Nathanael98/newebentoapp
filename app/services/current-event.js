import Service from '@ember/service';
import { computed } from '@ember/object';
import {inject as service} from "@ember/service";

export default Service.extend({
    routing: service('-routing'),


    token: computed('routing.currentRouteName', function(){
        return this.get('routing.currentRouteName')

    }),
});
