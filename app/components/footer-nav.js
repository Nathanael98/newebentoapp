import Component from '@ember/component';

export default Component.extend({
  
    router: Ember.inject.service('-routing'),


    actions: {

        contactosChat(id) {

            this.get('router').transitionTo("logged.contactos")

        }
    }

});
