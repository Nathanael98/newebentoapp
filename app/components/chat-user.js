import Component from '@ember/component';
import { validator, buildValidations } from "ember-cp-validations";
import $ from 'jquery';
import { inject as service } from '@ember/service';


const validations = buildValidations({

    //Respectivas validaciones para textArea del chat para que este no este vacio

    'textMensaje':
        validator('presence', {
            presence: true,
            message: 'Escribe texto primero'
        })
})


export default Component.extend(validations, {
    store: Ember.inject.service(),
    session: service(),


    //Cada vez que la vista se recarge 

    didRender() {

        //Se posiciona la vista al ultimo mensaje enviado
        var posicion = $("#up").offset().top;
        $("body").animate({ scrollTop: posicion + 100 }, 0);
        
        //Se actualizan todos los mensajes con esta enviado a estado leido
        var idMessage = $("#msj").text();
        if (idMessage) {
            this.store.findRecord('message', idMessage).then(newDato => {
                newDato.set('estado', "leido");
                newDato.save();
            })
        }
    },


    actions: {
        
        toggleError(attr) {
            switch (attr) {
                case 'textMensaje':
                    this.set('txtAreaErrorCheck', true)
                    break
            }
        },

        auto_grow() {

            //TO DO: Hacer el textArea Dimanico                        

        },

        auto() {

            //TO DO: Hacer el textArea Dimanico  
            
        },

        //Action para enviar mensajes
        createMensaje(chat) {

            //Se obtiene el uid del usuario authenticado
            let uid = this.get('session.currentUser.uid')

            //Buscamos el account que contenga el uid del usuario actual
            this.store.query('account', {
                orderBy: 'uid',
                equalTo: uid
            }).then((quer) => {
                let account = quer.get('firstObject')
                var userActual = account.id


                this.store.find('chat', chat).then((useChat) => {
                    this.store.findRecord('account', userActual).then((userEnvia) => {

                        //Obtenemos la fecha y hora de nuestra computadora para enviar el mensahje con esa descripcion
                        let hoy = new Date();
                        let fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + (hoy.getFullYear());
                        let hr = hoy.getHours() + ":" + hoy.getMinutes();
                        //let dateAndHour = fecha + " " + hr;

                        this.validate().then(({ validations }) => {
                            if (validations.get('isValid')) {

                                //despues de hacer la validaciom que el textArea no este vacio  se creara el nesaje
                                //y se guardara en firebase
                                let mensaje = this.store.createRecord('message', {
                                    message: this.textMensaje,
                                    hora: hr,
                                    fecha: fecha,
                                    estado: "enviado"
                                })

                                //El mensaje se asignara al Chat y al usuario que lo envia
                                useChat.get('messages').pushObject(mensaje)
                                userEnvia.get('messages').pushObject(mensaje)
                                mensaje.save().then(() => {

                                    //Despues de enviar el textArea mensaje se pone en blanco
                                    this.set('textMensaje', '')

                                    var posicion = $("#up").offset().top;
                                    $("body").animate({ scrollTop: posicion }, -10);


                                    useChat.save().then(() => {
                                        userEnvia.save().then(() => {


                                        })
                                    })

                                })

                            }

                        })
                    })
                })
            })
        },

        atras() {

            let uid = this.get('session.currentUser.uid')

            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((usuarioActual) => {

                usuarioActual.forEach(element => {

                    //obtengo el id de mi usuario autenticado
                    let idCuentaUser = element.id;

                    this.transitionToRoute("logged.contactos", idCuentaUser)

                })
            })

        },
    }
});