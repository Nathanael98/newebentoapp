import Component from '@ember/component';
import { inject as service } from '@ember/service';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import $ from 'jquery';


export default Component.extend(FindQuery, {
    store: Ember.inject.service(),
    session: service(),
    firebaseApp: service(),

    didInsertElement() {

        Ember.run.scheduleOnce('afterRender', this, function () {

            //Obtengo los ids del contacto u del evento
            var idContacto = $("#contacto").text();
            var idEvento = $("#evento").text();
            var msjNuevos = 0;

            //Obtengo el uid del usuario authenticado
            let uid = this.get('session.currentUser.uid')

            //Obtengo el id de la cuenta del usuario autenticado por medio de uid
            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((cuentaOrigen) => {

                cuentaOrigen.forEach(element => {

                    //obtengo todos los chats de mi usuario autenticado
                    element.get("chat").then((myChats) => {

                        //Si tengo chats
                        if (myChats.firstObject) {

                            //Recorro todos los chats existente del usuario authenticadp
                            myChats.forEach(elementChats => {

                                let idChat = elementChats.id

                                //consultar a mi contacto
                                this.store.find('account', idContacto).then(contact => {

                                    //obtengo los chats de mi contacto
                                    contact.get('chat').then(chatsContacto => {

                                        //Si el contacto tiene chats
                                        if (chatsContacto.firstObject) {

                                            //Recorro todos los chats
                                            chatsContacto.forEach(element => {

                                                let idChatContacto = element.id

                                                //comparo los chats en comun entre los dos usuarios (Authenticado, contacto)
                                                if (idChat == idChatContacto) {

                                                    //Buscar todos los chats de la base de datos del evento
                                                    this.store.query('chat', {
                                                        orderBy: "event",
                                                        equalTo: idEvento

                                                    }).then((chatsEvento) => {

                                                        //Si el evento tiene chats
                                                        if (chatsEvento.firstObject) {

                                                            //recorro todos los chats del evento
                                                            chatsEvento.forEach(element => {
                                                                let idChatsEvento = element.id;

                                                                //comparo los chats en comun de los usuarios con los del evento
                                                                if (idChat == idChatsEvento) {

                                                                    //Obtengo todos los mensajes con id del chat en comun de los usuarios y el evento
                                                                    this.store.query("message", {

                                                                        orderBy: "chat",
                                                                        equalTo: idChatsEvento

                                                                    }).then((messagesChat) => {

                                                                        //Recorro todos los mensajes del chat
                                                                        messagesChat.forEach(msjChat => {

                                                                            //Obtengo todos los mensajes que me envio mi contacto para despues hacer un contador 
                                                                            //de los mensajes recibidos

                                                                            this.store.query("message", {
                                                                                orderBy: "sender",
                                                                                equalTo: idContacto

                                                                            }).then((messagesContact) => {

                                                                                //Si ya hay un mensaje enviado en el chat
                                                                                if (messagesContact.firstObject) {

                                                                                    messagesContact.forEach(msjContacto => {

                                                                                        //obtener los mensajes que envio el contacto que pertenecen al chat y comparar
                                                                                        if (msjChat.id == msjContacto.id) {

                                                                                            //Busco los mensajes con estado enviado para desppues hacer un contador de los mesajes recibidos
                                                                                            this.store.query('message', {
                                                                                                orderBy: "estado",
                                                                                                equalTo: "enviado"
                                                                                            }).then((contarMensajes) => {

                                                                                                if (contarMensajes.firstObject) {

                                                                                                    contarMensajes.forEach(msjNoLeido => {

                                                                                                        //comparar los mensajes que envio el contacto con los mensajes con estado enviado 
                                                                                                        //para obtener el numero de mensajes recibidos

                                                                                                        if (msjChat.id == msjNoLeido.id) {

                                                                                                            msjNuevos++;

                                                                                                            //Inserto el numero de mensajes nuevos
                                                                                                            $("#text").text("mensajes");
                                                                                                            $("#recibidos").text(msjNuevos);

                                                                                                        } else {
                                                                                                            $("#recibidos").text("0");
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        } 

                                                                                    })
                                                                                } 
                                                                            })
                                                                        })
                                                                    })
                                                                }
                                                            })
                                                        } 
                                                    })
                                                } 
                                            })
                                        } 
                                    })
                                })
                            })
                        } 
                    })
                })
            })
        })
    },
});
