import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    firebaseApp: service(),



    /*----------------------------------------------------
        PRUEBA PARA CREAR ACCOUNS Y TOKENS FIREBASE
    ----------------------------------------------------*/

    actions: {

        //Se crea una cuenta con authenticacion en firebase y se guardan datos 
        registrarCuenta(email, contrasena) {

            //se crea una cuenta con correo y contraseña
            this.get('firebaseApp').auth().createUserWithEmailAndPassword(email, contrasena).then((user) => {
                
                var user = this.get('firebaseApp').auth().currentUser;
                user.sendEmailVerification().then(() => {

                    console.log('Enviando Correo...')
                    
                    if (user) {

                        //una vez creado el correo, se guardan los datos en la tabla accounts de este usuario 
                        //con una imagen por default que tieenen que estar cargada en storage de firebase


                        //
                        var cuenta = this.store.createRecord('account', {

                            name: this.nombre,
                            interes: "default",
                            img: "https://firebasestorage.googleapis.com/v0/b/ebento-26f43.appspot.com/o/imagenDefaul%2Fdefault.jpg?alt=media&token=fd99fe39-c6a7-4ced-9279-8fe5c97f8a9b",
                            nombreImg: "default",
                            email: this.correo,
                            pass: this.password,
                            uid: user.uid

                        })


                        //Se asigna el id de un evento estaticamente

                        let idEvento = '-LJKXIb84H44v9eF7nHZ'

                        //se busca ese evento
                        this.store.findRecord('evento', idEvento).then((event) => {

                            //Se crea el token
                            var codtoken = this.store.createRecord('token', {
                                codigo_token: this.token,

                            });

                            //Se relaciona la cuenta creada y el evento, al token
                            cuenta.get("tokens").pushObject(codtoken)
                            event.get("tokens").pushObject(codtoken)


                            //Se guardan las referencias 
                            //Asi queda -- Un token tiene una account y un evento
                            cuenta.save().then(() => {
                                codtoken.save().then(() => {
                                    event.save()
                                });
                            });
                        })
                    }



                }).catch(function (error) {
                    console.log(error)
                });
            }).catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log("Error" + errorCode)
                console.log("Error 2" + errorMessage)
            })
        },


        

        newToken() {

            let idEvento = '-LJKXN-3rjkUu1Ayhg1G'

            //busco el evento
            this.store.findRecord('evento', idEvento).then((event) => {

                //busco el account por email
                this.store.query('account', {
                    orderBy:"email",
                    equalTo: this.correo
                
                }).then((cuenta) => {

                    cuenta.forEach(element =>{

                        //creo el token nuevo, el cual contendra una account y un evento
                        var codtoken = this.store.createRecord('token', {
                            codigo_token: this.newtoken,
    
                        });
    
                        element.get("tokens").pushObject(codtoken)
                        event.get("tokens").pushObject(codtoken)
    
                        //asigno referencias  y guardo
                        element.save().then(() => {
                            codtoken.save().then(() => {
                                event.save()
                            });
                        });

                    })                    
                })
            })

        },
    }
});





