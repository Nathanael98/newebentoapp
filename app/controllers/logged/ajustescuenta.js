import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';
import logged from '../../routes/logged';

/* Validaciones... */
const Validations = buildValidations({


    'interes': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 40,
            message: 'No debe de excederce a mas de 40 caracteres'
        })
    ],


});


export default Controller.extend(Validations, {
    firebaseApp: service(),

    actions: {

        //Accion para mostrar el error del textArea del interes
        toogleError(attr) {
            switch (attr) {
                case 'interes':
                    this.set('interesErrorCheck', true)
                    break
            }
        },

        //Accion para cambiar la imagen del usuario authenticado
        NuevaImg(img) {

            if (img) {
                //Si hay una imagen seleccionada cambiara la imagen y la mostrara en la ruta
                var TmpPath = URL.createObjectURL(window.$('#userImage')[0].files[0]);
                window.$('#imageUser').attr("src", `${TmpPath}`)

                //muestra la ur (nombre de la imagen)
                $("#error").text(img)

            } else {

                //si no hay una imagen seleccionada imprime un error
                $("#error").text("Debes Seleccionar una imagen")

            }
        },

        //esta accion despliega el textArea para que el usuario pueda editar su interes
        editarInteres() {

            if ($('#interes').css('display') != 'inline') {
                $('#interes').css({ 'display': 'inline' })
                $('#btnEditar').css({ 'display': 'none' })
            }
        },

        //Oculta el textArea para que el usuario no actualize su interes
        cancelar() {

            $('#btnEditar').css({ 'display': 'inline' })
            $('#interes').css({ 'display': 'none' })
            $('#textInteres').val("");

        },

        //Actualiza el interes del usuario
        actualizarInteres(textInteres) {

            let uid = this.get('session.currentUser.uid')

            // Buscar la cuenta del usuario que esta autenticado
            this.store.query('account', {
                orderBy: 'uid',
                equalTo: uid                
            }).then((user) => {

                user.forEach((datoUser) => {

                    //actulizo el interes
                    datoUser.set('interes', textInteres);

                    
                    datoUser.save().then(() => {
                    //Despues de guardar el interes al usuario se le notifica que salio bien el proceso
                        swal(
                            'Felicidades!',
                            'Interes cambiado con exito!',
                            'success'
                        ).then((result) => {

                            //oculta el textArea
                            if (result.value) {
                                $('#btnEditar').css({ 'display': 'inline' })
                                $('#interes').css({ 'display': 'none' })
                                $('#textInteres').val("");
                            }
                        })
                    })//TO DO: se me paso meter un catch en caso que no se guarde el interes... que notifique al usuario
                })
            })
        },

        actualizarImg(IdUsu) {

            let nameImageUser;
            let image = document.getElementById('userImage');
            let uid = this.get('session.currentUser.uid')
            let file = image.files[0];

            //Si hay una imagen hace el proceso de actualizado de imagen
            if (file) {

                let metadata = {
                    'contentType': file.type
                };


                // Buscar la cuenta del usuario que esta autenticado
                this.store.query('account', {
                    orderBy: 'uid',
                    equalTo: uid
                }).then((userAuth) => {

                    userAuth.forEach((datos) => {
                        //Obtengo el nombre de la imagen guardada actualmente el firebase
                        nameImageUser = datos.nombreImg
                    })

                    //Validaciones para hacer el cambio de imagen de acuerdo a la situacion 
                    if (nameImageUser === "default") {

                        //Si el nombre de la imagen actual esta guardada como default

                        //Crea una ruta o referencia para guardar la imagen en el storage de firebase
                        let newRuta = this.get("firebaseApp").storage().ref().child(`imgUsers/${file.name}`).put(file, metadata);
                        newRuta.on('state_changed', null, (error) => {
                            console.error('Upload Failed:', error);
                        }, () => {

                            //En la variable newUrl se guarda la url de descarga de la imagen de firebase
                            let newUrl = newRuta.snapshot.metadata.downloadURLs[0];

                            //Actualiza los datos del usuario autenticado en este caso se actualiza el
                            //el nombre de la imagen y la url nueva
                            this.store.findRecord("account", IdUsu).then(newData => {
                                //La imagen se guarda con el nombre por viene cargado desde nuestra galeria
                                newData.set('nombreImg', `${file.name}`);
                                newData.set('img', newUrl);

                                newData.save().then(() => {

                                    //Despues de haber actualizado la imagen... se mostrara un sweetalert al usario
                                    //Diciendo que su proceso fue satisfactorio 
                                    swal(
                                        'Felicidades!',
                                        'Su imagen se cambio con exito!',
                                        'success'
                                    )
                                })
                            });
                        });
                    } else if (nameImageUser != "default") {

                        ///Eliminacion de La imagen Actual y se inserta la nueva imagen seleccionada

                        var urlDeletet = this.get("firebaseApp").storage().ref().child(`imgUsers/${nameImageUser}`);
                        urlDeletet.delete().then(function () {
                            //Aqui la imagen se elimino                        

                        }).catch(function (error) {
                            //Aqui la fallo eliminar imagen
                        });


                        //Crea una ruta o referencia para guardar la imagen en el storage de firebase
                        let newRuta = this.get("firebaseApp").storage().ref().child(`imgUsers/${file.name}`).put(file, metadata);

                        newRuta.on('state_changed', null, (error) => {
                            console.error('Upload Failed:', error);
                        }, () => {

                            //En la variable newUrl se guarda la url de descarga de la imagen de firebase
                            let newUrl = newRuta.snapshot.metadata.downloadURLs[0];

                            //Actualiza los datos del usuario autenticado en este caso se actualiza el
                            //el nombre de la imagen y la url nueva
                            this.store.findRecord("account", IdUsu).then(newData => {
                                //La imagen se guarda con el nombre por viene cargado desde nuestra galeria
                                newData.set('nombreImg', `${file.name}`);
                                newData.set('img', newUrl);



                                newData.save().then(() => {

                                     //Despues de haber actualizado la imagen... se mostrara un sweetalert al usario
                                    //Diciendo que su proceso fue satisfactorio
                                    swal(
                                        'Felicidades!',
                                        'Su imagen se cambio con exito!',
                                        'success'
                                    )
                                })
                            });
                        });
                    }
                })
            } else {
                //Si no hay imagen para actualizar mandará una alerta al usario
                //notificandole del error
                swal({
                    type: 'error',
                    title: 'Aún no has seleccionado una imagen',
                    text: 'Vuelve a intentarlo',
                    footer: ''
                })
            }
        },
    }
});
