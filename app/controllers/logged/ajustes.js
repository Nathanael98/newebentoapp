import Controller from '@ember/controller';
import { inject as service } from '@ember/service';


export default Controller.extend( {
    firebaseApp: service(),

    actions: {
        //Accion para cerrar session del usuario
        logout() {
            //sweet alert de confirmacion para cerrar session 
           swal({
               title: '¿Esta seguro de cerrar sesión?',
               text: 'Esta a punto de abandonar la APP',
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Aceptar',
               cancelButtonText: 'Cancelar'
           }).then((result) => {
       
               if (result.value) {
                   //si la respuesta es verdadera... la aplicacion no redireccionara a login
                   this.get('session').close().then(() => {
                       this.transitionToRoute('login')
                   })
               }
           });
       },       

       //Accion para acceder a la ruta de ajustescuenta y editar el perfil de el usuario authenticado 
        Ajustes() {

            let uid = this.get('session.currentUser.uid')

            //Consulto datos del account  por uid del usuario authenticado
            this.store.query('account', {

                orderBy: "uid",
                equalTo: uid

            }).then((cuentaOrigen) => {


                cuentaOrigen.forEach(element => {

                    //obtengo el id de mi usuario autenticado
                    let idCuentaUser = element.id;     
                    
                    //envio el id por url
                    this.transitionToRoute('logged.ajustescuenta', idCuentaUser)

                })
            })

        },

        //TO DO: Cambiar de lugar esta accion
        //Accion que envia a la ruta con toda la informacion del evento actual
        aboutEvento() {
            this.transitionToRoute('logged.detail-evento')
        }

    }
});
