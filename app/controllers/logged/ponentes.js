import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        //Accion para consultar el perfil del ponente del evento selecionado
        perfilPonente(ponente) {

            //Manda un id para consultar en el model, los datos del ponente seleccionado
            this.transitionToRoute("logged.detalleponentes", ponente)
            
        }
    }

});
