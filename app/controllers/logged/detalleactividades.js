import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['source'],
    source: 'logged.actividades',

    //TO DO:

    /* Se tendra acceso a esta ruta de dos maneras
   
    ------------------------------------------------------------------------------------------------------------
    
    1-- Por medio de la ruta logged.actividades quien muestra toda la informacion de las actividades
        del evento


        FUNCIONA

    ------------------------------------------------------------------------------------------------------------



    2-- por medio de la ruta home en donde se guardan aquellas actividades que nosotros queremos recordar
    

        NO FUNCIONA

    ------------------------------------------------------------------------------------------------------------



    esta ruta tiene query params el cual es usado para saber desde que ruta anterior se accedio 
    a esta ruta

    la ruta 1 recibe un id en la url (ruta dinamica) por lo tanto desde esta ruta (DETALLEACTIVIDADES)
    sabe que cuando regresa a la ruta logged.actividades debe regresar con un id


    la ruta 2 no recibe ninguna ruta dinamica -- aqui es donde marca error    
    
    */


    actions:{

        /*


        VERIFICAR HBS
        ------------------------------------------------------------------------------------------------------------
        cuando se accede desde la ruta 1 hay un boton el cual dice recordar color VERDE
        cuando se seleccione tiene que guardar la actividad en home para que el usuario 
        sepa a que actividad se inscribio




        cuando se accede desde la ruta 2 hay un boton el cual dice dejar de recordar color ROJO
        Este es por que se accede desde la ruta de home al seleccionar la actividad que anteriormente 
        por el usuario se habia inscrito

        ------------------------------------------------------------------------------------------------------------


        SOLUCION
        ------------------------------------------------------------------------------------------------------------

        ** arreglar rutas dimanicas

        ** O crear una nueva ruta para consultar las actividades (del evento y escojidas por el usuario) por separado

         */








         
        //TO DO: Accion del boton
        //Accion para guardar una actividad esojida por el usuario
        recordar(){

            swal(
                '',
                'Actividad agregada al recordatorio',
                'success'
            )

        }, 


        //TO DO: Accion del boton
        //Accion para eliminar una actividad esojida por el usuario
        dejarDeRecordar(){


                swal({
                title: '¿Dejar de recordar la actividad?',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            })


        }


    }
});
