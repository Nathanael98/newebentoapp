import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        //Accion para regresar a chat despues de visitar el perfil del contacto
        atras(idContacto, idChat) {
            
            //Pasando dos parametros que son idChaty idContacto
            this.transitionToRoute("logged.chatU", idContacto, idChat)

        }

    }
});
