import Controller from '@ember/controller';

export default Controller.extend({

    actions: {

        //accion para ver el perfil de mi contacto... se envia el id del contacto para despues consultar sus datos
        //se envia el id del chat por que este componente recibe un valor dinamico por url
        //ese es el id del chat
        //y el usuario cuando regrese a este componente no marque error

        perfil(idContacto, idChat) {

            this.transitionToRoute("logged.perfilcontacto", idContacto, idChat)

        }

    }

});
