import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('logged', { path: '/:token_id' }, function() {
    this.route('actividades', { path: 'actividades/:actividad' });
    this.route('detalleactividades', { path: 'detalleactividades/:actividad' });
    this.route('ponentes');
    this.route('detalleponentes', { path: 'perfil/:cid' });
    this.route('home');
    this.route('contactos');
    this.route('ajustes');
    this.route('ajustescuenta', { path: 'ajustescuenta/:c_id' });
    this.route('chatU', { path: 'chatU/:c_id/:c_user' });
    this.route('perfilcontacto', { path: 'contacto/:contact/:chat' });
    this.route('detail-evento');
  });
  this.route('prueba');
});

export default Router;

