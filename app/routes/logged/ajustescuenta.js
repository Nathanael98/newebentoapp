import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import DS from 'ember-data'
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';

export default Route.extend(FindQuery,{

    model(params) {

        return RSVP.hash({

            //Carga todos los datos de mi cuenta para poder editar el perfil
            cargarUsuario: DS.PromiseArray.create({
                promise: new Promise((resolve)=>{
                    
                    this.filterEqual(this.store, 'account', { 'id': params.c_id}, function (toms) {
                        console.log(toms)
                        return resolve(toms)                                                                               
                    })
                })
            })      


        })

    }

});
