import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
	ebentoApi: service(),

	model() {


		return RSVP.hash({

			//Se Muestra toda la informacion hacerca del evento como nombre, imagen, fecha de inicio,
			//fecha de clausura lugar etc

			about: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
				return this.get('ebentoApi').request('about').then((data) => {
					return data;
				})
			}),
			configurations: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
				return this.get('ebentoApi').request('configurations').then((data) => {
					return data;
				})
			}),
			location: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
				return this.get('ebentoApi').request('location').then((data) => {
					return data;

				})

			})	

		})
	}

});
