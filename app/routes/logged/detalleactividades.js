import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  ebentoApi: service(),


  queryParams: {
    source: {
      refreshModel: true
    }
  },

  model(params) {

    return RSVP.hash({

      //ID el cual fue seleccionado al filtrar por dia (ejemplo actividad1)
      //Entonces comparará la actividad1 con el resto de actividades y cuando coincidan 
      //se mostrara solo la informacion de dicha actividad
      actividad: params.actividad,


      //Muestra toda la informacion de las actividades del evento para hacer la comparacion
      schedule: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
        return this.get('ebentoApi').request('schedule').then((data) => {
          return data;
        })
      })

      //Se hace una comparacion de todas las actividades del evento que tengan este id para 
      //consultar la informacion detallada de solo una actividad

      //El Filtro se hace desde el hbs

    })

  }
});
