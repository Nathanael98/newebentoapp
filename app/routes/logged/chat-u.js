import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import DS from 'ember-data'

export default Route.extend(FindQuery,{

    session: service(),
    infinity: service(),

    model(params){

        let uid = this.get('session.currentUser.uid')
        
        return RSVP.hash({
 
            //Carga el chat y enviar parametro en accion para ver perfil de usuario
            cargarDatos: this.store.find('chat', params.c_user),

            //Cargar nombre de mi contacto seleccionado esto para poner en el header y saber con quien platico
            cargarNombre: this.store.find('account', params.c_id),     
            
            //TO DO: Infinity Scroll
            //cargarChat: this.infinity.model('chat'),

            //Carga el chat y los mensajes
            cargarChat: DS.PromiseArray.create({
                promise: new Promise((resolve)=>{
                    
                    this.filterEqual(this.store, 'chat', { 'id': params.c_user}, function (chatt) {                        
                        return resolve(chatt)                                                                               
                    })
                })
            }),

            //Cargar datos del usuario uthenticado esto para filtrar desde el hbs que usuario enviaba mensajes y quien es
            //el que recibe
            cargarDatosUsuario: this.store.query('account', {
				orderBy: "uid",
				equalTo: uid
			})
        })

    }
});
