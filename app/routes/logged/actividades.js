import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';
import DS from 'ember-data'

export default Route.extend({
	ebentoApi: service(),

	queryParams: {
		source: {
			refreshModel: true
		}
	},

	refreshModel: true,


	model(params) {

		return RSVP.hash({

			//Cuando el usuario da click en el boton del footer se carga las actividades del primer dia del evento por default 
			firstDay: this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
				return this.get('ebentoApi').request('schedule').then((data) => {

					let diaUno = data.firstObject;
					return diaUno;
				})
			}),

			//Obtiene todas las actividades del evento
			schedule: DS.PromiseArray.create({
				promise: new Promise((resolve) => {

					this.get('ebentoApi').initialize('zcP6jVFpr8WavZc8LvKucwDysLBQUNdS4t2kaMFGTDu47Xrc', '5aa1807098759fc51bf9ea43').then(() => {
						return this.get('ebentoApi').request('schedule').then((data) => {

							return resolve(data);

						})
					})
				})
			}),

			//Obtiene el valor dinamico traido desde la url
			fecha: params.actividad

			/* Se hace un filtro de las actividades por dia

	  dicho filtro se hace desde el hbs con {{#if (eq) }} etc
	  

	  se buscan las actividades ((schedule)) que pertenezcan al id ((fecha))
          
      */
		})
	}

});
