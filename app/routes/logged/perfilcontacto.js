import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

    model(params){

        return RSVP.hash({

        //Obtengo el account por medio del id del contacto seleccionado
        contacto: this.store.find('account', params.contact),
        // Obtengo el chat por medio del id
        chat: this.store.find('chat', params.chat),

        })

    }
});
