import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';


export default Route.extend({

	session: service(),
	beforeModel() {
		return this.get('session').fetch().then(this.checkAuth.bind(this)).catch(this.checkAuth.bind(this))
	},
	checkAuth() {

		//si el usuario no está authenticado no podra acceder a ninguna ruta hija de logged
		if (!this.get('session.isAuthenticated')) {
			return this.transitionTo('login')		
		}
	},

	//Obtiene el id del token enviado por urldesde la ruta login, esto para hacer una consulta de datos 
	model(params) {

		//Obtenemos el uid del usuario actualmente autenticado
		let uid = this.get('session.currentUser.uid')		

		//Retornamos un arreglo de objetos con RSPV
		return RSVP.hash({

			//Hacemos la busqueda todos los datos del usuario autenticado de acuerdo al uid
			cargarDatosUsuario: this.store.query('account', {
				orderBy: "uid",
				equalTo: uid			
			}),		
			
			//Obtenemos los datos del token
			token: this.store.find('token', params.token_id)
		})
	}
});
